import React, { useState,useEffect } from "react";
import service from '../services/service'
import Card from 'react-bootstrap/Card';
import Moment from 'moment';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import ListGroup from 'react-bootstrap/ListGroup';

const Content = () => {

  const [listCountries, setListCountries] = useState([
    {
      label:'Brasil',
      value:'BR'
    },
    {
      label:'China',
      value:'CN'
    },
    {
      label:'Alemania',
      value:'DE'
    },
    {
      label:'Rusia',
      value:'RU'
    },
    {
      label:'Francia',
      value:'FR'
    },
    {
      label:'Argentina',
      value:'AR'
    },
    {
      label:'Colombia',
      value:'CO'
    },
    {
      label:'Japón',
      value:'JP'
    },
    {
      label:'Venezuela',
      value:'VE'
    },
    {
      label:'Egipto',
      value:'EG'
    },
    {
      label:'Korea',
      value:'KR'
    },
    {
      label:'Estados unidos',
      value:'US'
    },
  ])

  const [listCities, setListCities] = useState([
    {
      label:'Brasilia',
      value:'brasilia'
    },
    {
      label:'Pekin',
      value:'pekin'
    },
    {
      label:'Berlin',
      value:'berlin'
    },
    {
      label:'Moscu',
      value:'moscu'
    },
    {
      label:'Paris',
      value:'paris'
    },
    {
      label:'Buenos aires',
      value:'buenos aires'
    },
    {
      label:'Bogota',
      value:'bogota'
    },
    {
      label:'Tokio',
      value:'tokio'
    },
    {
      label:'Caracas',
      value:'caracas'
    },
    {
      label:'Cairo',
      value:'cairo'
    },
    {
      label:'Seul',
      value:'seul'
    },
    {
      label:'New York',
      value:'new york'
    },
  ])

  const [letterCountry, setLetterCountry] = useState()
  const [disabledButton, setDisabledButton] = useState(true)
  const [articles, setArticles] = useState([])
  const [showNews, setShowNews] = useState(false)
  const [city, setCity] = useState()
  const [disabledButtonCity, setDisabledButtonCity] = useState(true)
  const [results, setResults] = useState([])
  const [showNewsCity, setShowNewsCity] = useState(false)
  const [weather, setWeather] = useState()
  const [cityW, setCityW] = useState()
  const [showHistory, setShowHistory] = useState(false)
  const [history, setHistory] = useState([])

  const handleCountry= (e) => {
    if(e.target.value !== ''){
      setLetterCountry(e.target.value);
      setDisabledButton(false)
    }else{
      setDisabledButton(true)
    }
  }

  const consultNews = () =>{
    service.getNewsByCountry(letterCountry).then(res=>{
      setArticles(res.articles)
      setShowNews(true)
    })
  }

  const linkNew = (link) => {
    window.open(link, '_blank', 'noopener,noreferrer');
  }

  const handleCity= (e) => {
    if(e.target.value !== ''){
      setCity(e.target.value);
      setDisabledButtonCity(false)
    }else{
      setDisabledButtonCity(true)
    }
  }
  const consultNewsCity = () =>{
    service.getNewsByCity(city).then(res=>{
      setResults(res.results)
      setShowNewsCity(true)
    })
    service.getCoordsByCity(city).then(res=>{
      service.getWeather(res[0].lat,res[0].lon).then(res=>{
        setWeather(res.weather[0].description)
        setCityW(city)
        service.postHistory(city,res.weather[0].description)
      })
    })
  }

  const consultHistory = () => {
    service.getHistory().then(res=>{
      setHistory(res)
      setShowHistory(true)
    })
  }

  return (
    <>
      <div className="container">
        <Tabs
          defaultActiveKey="city"
          id="uncontrolled-tab-example"
          className="mb-3 mt-3"
        >
          <Tab eventKey="city" title="Cities">
            <h5>Select a city</h5>
            <select className="form-select" aria-label="Default select example" onChange={(e)=>handleCity(e)}>
              <option value={''}>Selecct...</option>
              {
                listCities.map((city,index)=>{
                  return(
                    <option key={index} label={city.label} value={city.value}></option>
                  )
                })
              }
              
              
            </select>
            <button disabled={disabledButtonCity} type="button" className="btn btn-primary mt-3" onClick={consultNewsCity}>Consult</button>
            {
              showNewsCity &&
              <div className="container mt-3">
                <Card style={{ width: '20rem'}}>
                  <Card.Body>
                    <Card.Title className="mb-4 title upper">{cityW}</Card.Title>
                    <Card.Subtitle className="sub-title  mb-2">Weather: {weather}</Card.Subtitle>
                  </Card.Body>
                </Card>
                <div className="row row-cols-4 align-items-start">
                {
                  results.length > 0 &&
                    results.map((res,i)=>{
                      return(
                        <div key={i} className="col mt-2 mb-4">
                          <Card style={{ width: '18rem',cursor:'pointer' }} onClick={()=>linkNew(res.link)}>
                            <Card.Img variant="top" src={res.image_url} />
                            <Card.Body>
                              <Card.Title className="mb-4 title">{res.title}</Card.Title>
                              <Card.Subtitle className="sub-title  mb-2">{res.creator && res.creator[0]}/{Moment(res.pubDate).format("DD-MM-YYYY")}</Card.Subtitle>
                              <Card.Text className="text">
                                {res.description}
                              </Card.Text>
                            </Card.Body>
                          </Card>
                        </div>
                      )
                    })
                }
                </div>
              </div>  
            }
          </Tab>
          <Tab eventKey="country" title="Countries">
            <h5>Select a country</h5>
            <select className="form-select" aria-label="Default select example" onChange={(e)=>handleCountry(e)}>
              <option value={''}>Selecct...</option>
              {
                listCountries.map((country,index)=>{
                  return(
                    <option key={index} label={country.label} value={country.value}></option>
                  )
                })
              }
              
              
            </select>
            <button disabled={disabledButton} type="button" className="btn btn-primary mt-3" onClick={consultNews}>Consult</button>
            {
              showNews &&
              <div className="container mt-3">
                <div className="row row-cols-4 align-items-start">
                {
                  articles.length > 0 &&
                    articles.map((art,i)=>{
                      return(
                        <div key={i} className="col mt-2 mb-4">
                          <Card style={{ width: '18rem',cursor:'pointer' }} onClick={()=>linkNew(art.url)}>
                            <Card.Img variant="top" src={art.urlToImage} />
                            <Card.Body>
                              <Card.Title className="mb-4 title">{art.title}</Card.Title>
                              <Card.Subtitle className="sub-title  mb-2">{art.author}/{Moment(art.publishedAt).format("DD-MM-YYYY")}</Card.Subtitle>
                              <Card.Text className="text">
                                {art.description}
                              </Card.Text>
                            </Card.Body>
                          </Card>
                        </div>
                      )
                    })
                }
                </div>
              </div>  
            }
          </Tab>
          <Tab eventKey="history" title="History">
          <h5>Check search history</h5>
          <button type="button" className="btn btn-primary mt-3" onClick={consultHistory}>Consult</button>
          {
            showHistory && 

              <ListGroup className="mt-3">
                {
                  history.length > 0 &&
                    history.map((h)=>{
                      return(
                        <ListGroup.Item>{h.city}/{h.info}</ListGroup.Item>
                      )
                    })
                }
              </ListGroup>
          }
          </Tab>
          
        </Tabs>
      </div>
        
    </>
  );
};

export default Content;