import React, { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

const Navbar = () => {

  return (
    <nav className="navbar">
      <h4 className="navbar-logo">
        Tu PruTec 
      </h4>
      <div className="nav-menu">
        <h6>USER NAME <FontAwesomeIcon color="#fff" icon={faChevronDown} /></h6>
      </div>
    </nav>
  );
};

export default Navbar;