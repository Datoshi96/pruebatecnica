import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import Content from './views/Content';

function App() {
  return (
    <div className="App">
      <Navbar></Navbar>
      <Content></Content>
    </div>
  );
}

export default App;
