import axios from 'axios';


export default {
    getNewsByCountry : (country) => {
        const response = axios.get(`https://newsapi.org/v2/top-headlines?country=${country}&apiKey=a70b1d017f6a406693abdb4135c136d4`).then( res =>{
            return res.data;
        }).catch(error => {
            console.log(error);
            return error;
        })
        return response
    },

    getNewsByCity : (city) => {
        const response = axios.get(`https://newsdata.io/api/1/news?apikey=pub_1212377e9c478dc017c5ffc1b93bb3db7a88a&q=${city}`).then( res =>{
            return res.data;
        }).catch(error => {
            console.log(error);
            return error;
        })
        return response
    },

    getCoordsByCity : (city) => {
        const response = axios.get(`http://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=1&appid=a451b279b196af677d15e2dfdca7178b`).then( res =>{
            return res.data;
        }).catch(error => {
            console.log(error);
            return error;
        })
        return response
    },

    getWeather: (lat,lon) => {
        const response = axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=a451b279b196af677d15e2dfdca7178b`).then( res =>{
            return res.data;
        }).catch(error => {
            console.log(error);
            return error;
        })
        return response
    },

    getHistory: () => {
        const response = axios.get(`https://localhost:44355/api/history`).then( res =>{
            return res.data;
        }).catch(error => {
            console.log(error);
            return error;
        })
        return response
    },

    postHistory: (place,info) => {
        let data={
            city:place,
            info:info,
        }
        const response = axios.post(`https://localhost:44355/api/history`,data).then( res =>{
            console.log(res.data)
            return res.data;
        }).catch(error => {
            return error;
        })
        return response
    },

}